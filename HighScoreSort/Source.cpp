#include <iostream>
#include <fstream>
#include <string>
#include <vector>

struct HighScore
{
	std::string name;
	int score;
};
void WriteScores();
void ReadScores();
void Sort(std::vector<int> &a);
void SortHighScores(std::vector<HighScore> &a);

int main()
{
	std::cout << "Press any key to write scores" << std::endl;
	int _iTemp;
	std::cin >> _iTemp;
	WriteScores();
	std::cout << "Test scores saved! Press enother key to test reading and sorting scores." << std::endl;
	std::cin >> _iTemp;
	ReadScores();
	std::cin >> _iTemp;
	return (0);
}

void WriteScores()
{
	std::ofstream myFile;

	const int kiMaxScores = 3;
	std::string highscoreNames[kiMaxScores];
	int highscoreScores[kiMaxScores];

	// Setup Test Data.
	highscoreNames[0] = "Mark";
	highscoreScores[0] = 400;
	highscoreNames[1] = "Alistair";
	highscoreScores[1] = 200;
	highscoreNames[2] = "Kevin";
	highscoreScores[2] = 6000;

	//Write the score file to disk.
	myFile.open("scores.txt");

	if (myFile.is_open())
	{
		for (int i = 0; i < kiMaxScores; ++i)
		{
			myFile << highscoreNames[i] << "=";
			myFile << highscoreScores[i] << std::endl;
		}

		myFile.close();
	}

}



void ReadScores()
{
	std::ifstream myFile;

	myFile.open("scores.txt");

	if (myFile.is_open())
	{
		std::string strLine;
		std::vector<HighScore> scores;

		while (!myFile.eof())
		{
			std::getline(myFile, strLine);

			size_t equalsPos = strLine.find('=');

			std::string strName = strLine.substr(0, equalsPos);
			std::string strScore = strLine.substr(equalsPos + 1,
				strLine.length());

			int iScore = atoi(strScore.c_str()); //Converts strings to ints
			std::cout << strName << " = " << iScore << std::endl; //Prints scores to COUT.

			//Then put the score in the highscore table...
			HighScore newHS;
			newHS.name = strName;
			newHS.score = iScore;
			scores.push_back(newHS);
			
		}

		myFile.close();

		SortHighScores(scores);

		for (size_t i = 0; i < scores.size(); ++i) {
			std::cout << scores[i].name << " " << scores[i].score << '\n';
		}
	}
}

void SortVector(std::vector<int> &a) {
	for (size_t i = 1; i < a.size(); ++i) {
		for (size_t j = 0; j < a.size() - 1; ++j) {
			if (a[j] > a[i]) std::swap(a[j], a[i]);
		}
	}
}

void SortHighScores(std::vector<HighScore> &a) {
	for (size_t i = 1; i < a.size(); ++i) 
	{
		for (size_t j = 0; j < a.size() - 1; ++j)
		{
			if (a[j].score > a[i].score)
			{
				std::swap(a[j], a[i]);
			}
		}
	}
}
